"""
@author: Mahmmad Armoun
"""

import numpy as np
import os.path
import cv2
import time
import argparse
import os.path as osp

from sort import Sort
from Detector.detector import Detector
import re
#from detector import GroundTruthDetections

def main():
    cam = cv2.VideoCapture(0)
    images = '/home/mohammad/MyProjects/master_project/2_Dataset/MyData/frames' #'/home/mohammad/MyProjects/master_project/1_MyImplementation/experimenting-with-sort/test' #'/home/mohammad/MyProjects/master_project/2_Dataset/data_tracking_image_2/training/image_02/0016' 
    args = parse_args()
    display = args.display
    use_dlibTracker  = args.use_dlibTracker
    saver = args.saver

    total_time = 0.0
    total_frames = 0

    # for disp
    if display:
        colours = np.random.uniform(low=0, high=255, size=(32, 3)).astype(int) # used only for display


    if not os.path.exists('output'):
        os.makedirs('output')
    out_file = 'output/townCentreOut.top'

    #init detector
    detector = Detector()

    #init tracker
    tracker =  Sort(use_dlib= use_dlibTracker) #create instance of the SORT tracker

    if use_dlibTracker:
        print ("Dlib Correlation tracker activated!")
    else:
        print ("Kalman tracker activated!")

    with open(out_file, 'w') as f_out:
        
        imlist = [osp.join(osp.realpath('.'), images, img) for img in os.listdir(images) if os.path.splitext(img)[1] == '.png' or os.path.splitext(img)[1] =='.jpeg' or os.path.splitext(img)[1] =='.jpg']
        imlist.sort(key=lambda f: int(re.sub('\D', '', f)))
        cnt = 0
        for frame in imlist:  
            cnt +=1
            # get detections
            img = cv2.imread(frame)
            # ret, img = cam.read()
            # if not ret:
            #     print("failed to grab frame")
            #     continue
            detections = detector.detect(img)
            
            if detections is None:
                continue
            
            if (display):
                if(use_dlibTracker):
                    title= "Dlib Correlation Tracker"
                else:
                    title= "Kalman Tracker"

            start_time = time.time()
            #update tracker
            trackers = tracker.update(detections,None)

            cycle_time = time.time() - start_time
            total_time += cycle_time

            print('frame: %d...took: %3fs'%(cnt,cycle_time))

            for d in trackers:
                f_out.write('%d,%d,%d,%d,x,x,x,x,%.3f,%.3f,%.3f,%.3f\n' % (d[4], cnt, 1, 1, d[0], d[1], d[2], d[3]))
                if (display):
                    d = d.astype(np.int32)
                    pt1 = (d[0], d[1])
                    pt2 = (d[2], d[3])
                    clr = ((int)(colours[d[4] % 32, 0]), (int)(colours[d[4] % 32, 1]), (int)(colours[d[4] % 32, 2]))
                    cv2.rectangle(img,pt1 , pt2, clr, 2)
                    label = 'id = %d' % (d[4])
                    t_size = cv2.getTextSize(label, cv2.FONT_HERSHEY_PLAIN, 1 , 1)[0]
                    c2 = d[0] + t_size[0] + 3, d[1] + t_size[1] + 4
                    cv2.putText(img, label, (d[0], d[1] + t_size[1] + 4), cv2.FONT_HERSHEY_PLAIN, 1, [225,255,255], 1)
                    
            if detections != []:#detector is active in this frame
                print("DETECTOR")
                label = 'DETECTOR'
                t_size = cv2.getTextSize(label, cv2.FONT_HERSHEY_PLAIN, 1 , 1)[0]
                cv2.putText(img, label, (10,10 + t_size[1] + 4), cv2.FONT_HERSHEY_PLAIN, 1.6, [0,0,0], 1)

            if (display):
                #smaller = cv2.resize(img, (0,0), fx=0.5, fy=0.5) 
                cv2.imshow("img" ,img)
                
                
                #save the frame with tracking boxes
                if(saver):
                    fig.savefig("./frameOut/f%d.jpg"%(cnt+1),dpi = 200)

            if cv2.waitKey(1) == 27:
                break




    print("Total Tracking took: %.3f for %d frames or %.1f FPS"%(total_time,total_frames,total_frames/total_time))

def parse_args():
    """Parse input arguments."""
    parser = argparse.ArgumentParser(description='Experimenting Trackers with SORT')
    parser.add_argument('--NoDisplay', dest='display', help='Disables online display of tracker output (slow)',action='store_false')
    parser.add_argument('--dlib', dest='use_dlibTracker', help='Use dlib correlation tracker instead of kalman tracker',action='store_true')
    parser.add_argument('--save', dest='saver', help='Saves frames with tracking output, not used if --NoDisplay',action='store_true')

    args = parser.parse_args()
    return args

if __name__ == '__main__':
    main()
