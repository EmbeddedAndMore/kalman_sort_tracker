from __future__ import division
import time
import torch 
import torch.nn as nn
from torch.autograd import Variable
import numpy as np
import cv2 
from .util import *
import argparse
import os 
import os.path as osp
from .darknet import Darknet
from .preprocess import prep_image, inp_to_image
import pandas as pd
import random 
import pickle as pkl
import itertools


class Detector:
    def __init__(self):
        self.scales = '1,2,3'
        self.batch_size = 1
        self.confidence = 0.1
        self.nms_thesh = 0.4

        self.CUDA = torch.cuda.is_available()

        self.num_classes = 80
        self.classes = load_classes('Detector/data/coco.names')


        print("Loading network.....")
        self.model = Darknet('Detector/cfg/yolov3-tiny.cfg')
        self.model.load_weights('Detector/yolov3-tiny.weights')
        print("Network successfully loaded")

        self.model.net_info["height"] = '416'
        self.inp_dim = int(self.model.net_info["height"])

        # error if size was not multiple of 32
        assert self.inp_dim % 32 == 0 
        assert self.inp_dim > 32

         #If there's a GPU availible, put the model on GPU
        if self.CUDA:
            self.model.cuda()

        #Set the model in evaluation mode
        self.model.eval()


    def detect(self, orig_img):

        self.original_image = orig_img
        start = time.time()
        batch = prep_image(orig_img, self.inp_dim)

        im_batch = batch[0]
        orig_ims = batch[1]
        im_dim_list = batch[2]
        im_dim_list = torch.FloatTensor(im_dim_list).repeat(1,2)

        if self.CUDA:
            im_dim_list = im_dim_list.cuda()
    
        leftover = 0
        
        if (len(im_dim_list) % self.batch_size):
            leftover = 1
            
        
        i = 0


        objs = {}


        start_detect = time.time()
        if self.CUDA:
            batch = batch.cuda()

        with torch.no_grad():
            prediction = self.model(Variable(im_batch), self.CUDA)


        prediction = write_results(prediction, self.confidence, self.num_classes, nms = True, nms_conf = self.nms_thesh)
        
        
        
        if type(prediction) == int:
            i += 1
            return None

        size = prediction.shape

        end = time.time()
        
        prediction[:,0] += i * self.batch_size
        

        #consider just "person" class => prediction[:,-1] == 0
        prediction = prediction[prediction[:,-1]== 0,:]
        #for im_num, image in enumerate(imlist[i*batch_size: min((i +  1)*batch_size, len(imlist))]):
            #im_id = i*batch_size + im_num
        objs = [self.classes[int(x[-1])] for x in prediction]
        print("predicted in {0:6.3f} seconds".format((end - start_detect)/self.batch_size))
        print("{0:20s} {1:s}".format("Objects Detected:", " ".join(objs)))
        print("----------------------------------------------------------")


            # file_detected.write(f'{img_counter} {obj_counter} {}')
        i += 1

        
        if self.CUDA:
            torch.cuda.synchronize()

        im_dim_list = torch.index_select(im_dim_list, 0, prediction[:,0].long())
    
        scaling_factor = torch.min(self.inp_dim/im_dim_list,1)[0].view(-1,1)
        
        
        prediction[:,[1,3]] -= (self.inp_dim - scaling_factor*im_dim_list[:,0].view(-1,1))/2
        prediction[:,[2,4]] -= (self.inp_dim - scaling_factor*im_dim_list[:,1].view(-1,1))/2
        
        
        
        prediction[:,1:5] /= scaling_factor
        
        for i in range(prediction.shape[0]):
            prediction[i, [1,3]] = torch.clamp(prediction[i, [1,3]], 0.0, im_dim_list[i,0])
            prediction[i, [2,4]] = torch.clamp(prediction[i, [2,4]], 0.0, im_dim_list[i,1])

        
        resize_time = time.time()

        print(f'whole detection duration: {resize_time - start}')

        # predictions = []
        # for pred in prediction:
        #     predictions.append(pred[1:5].int().)

        predictions = prediction[:,1:5].int().numpy()
        return predictions


