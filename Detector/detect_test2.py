from detector import Detector
import cv2

detec = Detector()

img = cv2.imread('dog-cycle-car.png')

detections = detec.detect(img)

print(detections)