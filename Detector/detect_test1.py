from __future__ import division
import time
import torch 
import torch.nn as nn
from torch.autograd import Variable
import numpy as np
import cv2 
from util import *
import argparse
import os 
import os.path as osp
from darknet import Darknet
from preprocess import prep_image, inp_to_image
import pandas as pd
import random 
import pickle as pkl
import itertools


class test_net(nn.Module):
    def __init__(self, num_layers, input_size):
        super(test_net, self).__init__()
        self.num_layers= num_layers
        self.linear_1 = nn.Linear(input_size, 5)
        self.middle = nn.ModuleList([nn.Linear(5,5) for x in range(num_layers)])
        self.output = nn.Linear(5,2)
    
    def forward(self, x):
        x = x.view(-1)
        fwd = nn.Sequential(self.linear_1, *self.middle, self.output)
        return fwd(x)
        
def get_test_input(input_dim, CUDA):
    img = cv2.imread("dog-cycle-car.png")
    img = cv2.resize(img, (input_dim, input_dim)) 
    img_ =  img[:,:,::-1].transpose((2,0,1))
    img_ = img_[np.newaxis,:,:,:]/255.0
    img_ = torch.from_numpy(img_).float()
    img_ = Variable(img_)
    
    if CUDA:
        img_ = img_.cuda()
    num_classes
    return img_

colors = pkl.load(open("pallete", "rb"))




def arg_parse():
    """
    Parse arguements to the detect module
    
    """
    image_dir = '/home/mohammad/MyProjects/master_project/2_Dataset/data_tracking_image_2/training/image_02/0016'
    
    parser = argparse.ArgumentParser(description='YOLO v3 Detection Module')
   
    parser.add_argument("--images", dest = 'images', help = 
                        "Image / Directory containing images to perform detection upon",
                        default = image_dir, type = str)
    parser.add_argument("--det", dest = 'det', help = 
                        "Image / Directory to store detections to",
                        default = "det", type = str)
    parser.add_argument("--bs", dest = "bs", help = "Batch size", default = 1)
    parser.add_argument("--confidence", dest = "confidence", help = "Object Confidence to filter predictions", default = 0.3)
    parser.add_argument("--nms_thresh", dest = "nms_thresh", help = "NMS Threshhold", default = 0.4)
    parser.add_argument("--cfg", dest = 'cfgfile', help = 
                        "Config file",
                        default = "cfg/yolov3-tiny.cfg", type = str)
    parser.add_argument("--weights", dest = 'weightsfile', help = 
                        "weightsfile",
                        default = "yolov3-tiny.weights", type = str)
    parser.add_argument("--reso", dest = 'reso', help = 
                        "Input resolution of the network. Increase to increase accuracy. Decrease to increase speed",
                        default = "416", type = str)
    parser.add_argument("--scales", dest = "scales", help = "Scales to use for detection",
                        default = "1,2,3", type = str)
    
    return parser.parse_args()



if __name__ ==  '__main__':
    args = arg_parse()
    
    scales = args.scales
    
    
#        scales = [int(x) for x in scales.split(',')]
#        
#        
#        
#        args.reso = int(args.reso)
#        
#        num_boxes = [args.reso//32, args.reso//16, args.reso//8]    
#        scale_indices = [3*(x**2) for x in num_boxes]
#        scale_indices = list(itertools.accumulate(scale_indices, lambda x,y : x+y))
#    
#        
#        li = []
#        i = 0
#        for scale in scale_indices:        
#            li.extend(list(range(i, scale))) 
#            i = scale
#        
#        scale_indices = li

    # file_detected = open("detected.txt")
    # img_counter = 0
    # obj_counter = 0

    images = args.images
    batch_size = int(args.bs)
    confidence = float(args.confidence)
    nms_thesh = float(args.nms_thresh)
    start = 0

    CUDA = torch.cuda.is_available()

    num_classes = 80
    classes = load_classes('data/coco.names') 

    #Set up the neural network
    print("Loading network.....")
    model = Darknet(args.cfgfile)
    model.load_weights(args.weightsfile)
    print("Network successfully loaded")
    
    model.net_info["height"] = args.reso
    inp_dim = int(model.net_info["height"])
    assert inp_dim % 32 == 0 
    assert inp_dim > 32

    #If there's a GPU availible, put the model on GPU
    if CUDA:
        model.cuda()
    
    
    #Set the model in evaluation mode
    model.eval()
    
    read_dir = time.time()
    #Detection phase
    try:
        imlist = [osp.join(osp.realpath('.'), images, img) for img in os.listdir(images) if os.path.splitext(img)[1] == '.png' or os.path.splitext(img)[1] =='.jpeg' or os.path.splitext(img)[1] =='.jpg']
        imlist.sort()
    except NotADirectoryError:
        imlist = []
        imlist.append(osp.join(osp.realpath('.'), images))
    except FileNotFoundError:
        print ("No file or directory with the name {}".format(images))
        exit()
        
    if not os.path.exists(args.det):
        os.makedirs(args.det)

    
        
    start_det_loop = time.time()
    for im in imlist:
        loop_start = time.time()
        orig_img = cv2.imread(im)
        batch = prep_image(orig_img, inp_dim)
        
        im_batch = batch[0]
        orig_ims = batch[1]
        im_dim_list = batch[2]
        im_dim_list = torch.FloatTensor(im_dim_list).repeat(1,2)
        
        if CUDA:
            im_dim_list = im_dim_list.cuda()
    
        leftover = 0
        
        if (len(im_dim_list) % batch_size):
            leftover = 1
            
        
        i = 0
        
        #model(get_test_input(inp_dim, CUDA), CUDA)

        
        
        objs = {}


        start = time.time()
        if CUDA:
            batch = batch.cuda()

        with torch.no_grad():
            prediction = model(Variable(im_batch), CUDA)


        prediction = write_results(prediction, confidence, num_classes, nms = True, nms_conf = nms_thesh)
        
        size = prediction.shape
        
        if type(prediction) == int:
            i += 1
            continue

        end = time.time()
        
                    
#        print(end - start)

        prediction[:,0] += i * batch_size
        
    
        #for im_num, image in enumerate(imlist[i*batch_size: min((i +  1)*batch_size, len(imlist))]):
            #im_id = i*batch_size + im_num
        objs = [classes[int(x[-1])] for x in prediction]
        print("{0:20s} predicted in {1:6.3f} seconds".format(im.split("/")[-1], (end - start)/batch_size))
        print("{0:20s} {1:s}".format("Objects Detected:", " ".join(objs)))
        print("----------------------------------------------------------")


            # file_detected.write(f'{img_counter} {obj_counter} {}')
        i += 1

        
        if CUDA:
            torch.cuda.synchronize()

        im_dim_list = torch.index_select(im_dim_list, 0, prediction[:,0].long())
    
        scaling_factor = torch.min(inp_dim/im_dim_list,1)[0].view(-1,1)
        
        
        prediction[:,[1,3]] -= (inp_dim - scaling_factor*im_dim_list[:,0].view(-1,1))/2
        prediction[:,[2,4]] -= (inp_dim - scaling_factor*im_dim_list[:,1].view(-1,1))/2
        
        
        
        prediction[:,1:5] /= scaling_factor
        
        for i in range(prediction.shape[0]):
            prediction[i, [1,3]] = torch.clamp(prediction[i, [1,3]], 0.0, im_dim_list[i,0])
            prediction[i, [2,4]] = torch.clamp(prediction[i, [2,4]], 0.0, im_dim_list[i,1])

        
        resize_time = time.time()

        def write(x, batches, results):
            c1 = tuple(x[1:3].int())
            c2 = tuple(x[3:5].int())
            img = results
            cls = int(x[-1])
            label = "{0}".format(classes[cls])
            if label != 'person':
                return img
            if c2[0] - c1[0] < 20:
                return img
            
            if c2[1] - c1[1] < 40:
                return img
            color = random.choice(colors)
            cv2.rectangle(img, c1, c2,color, 1)
            cv2.circle(img,((c1[0] + c2[0])/2,(c1[1] + c2[1])/2),3,color,2)
            t_size = cv2.getTextSize(label, cv2.FONT_HERSHEY_PLAIN, 1 , 1)[0]
            c2 = c1[0] + t_size[0] + 3, c1[1] + t_size[1] + 4
            cv2.rectangle(img, c1, c2,color, -1)
            cv2.putText(img, label, (c1[0], c1[1] + t_size[1] + 4), cv2.FONT_HERSHEY_PLAIN, 1, [225,255,255], 1)
            return img
          
        
        list(map(lambda x: write(x, im_batch, orig_ims), prediction))
        draw_time = time.time()
        #copied = np.copy(orig_ims)
        cv2.imshow("detected",orig_ims)
        loop_end = time.time()
        print(f'loading duration: {start - loop_start}')
        print(f'resize  duration: {resize_time - end}')
        print(f'draw    duration: {draw_time - resize_time}')
        print(f'loop    duration: {loop_end - loop_start}')
        if cv2.waitKey(1) == 27:
            break
        

    #cv2.waitKey(0)